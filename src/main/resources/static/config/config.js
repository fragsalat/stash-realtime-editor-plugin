require([
    'jquery',
    'stash/api/util/navbuilder',
    'stash/api/util/server',
    'stash/api/util/state'
], function(
    $,
    navbuilder,
    server,
    state
) {
    $(document).ready(function() {
        var $collaborationEnabled = $('#collaboration-enabled');
        $collaborationEnabled.on('change', function(e) {
            var xhr = server.rest({
                type : $collaborationEnabled[0].checked ? 'PUT' : 'DELETE',
                url : navbuilder.pluginServlets().path('editor-settings', 'projects', state.getProject().key, 'repos', state.getRepository().slug).build()
            });

            var $spinner = $('<div class="checkbox-spinner"></div>');
            $spinner.insertAfter($collaborationEnabled);
            $collaborationEnabled.hide();
            $spinner.spin();

            xhr.always(function() {
                $collaborationEnabled.show();
                $spinner.remove();
            }).fail(function() {
                $collaborationEnabled.val(!$collaborationEnabled.val());
            });
        });
    });
});