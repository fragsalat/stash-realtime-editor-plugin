package com.atlassian.stash.plugin.editor;

import com.atlassian.fugue.Either;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ServletSupport {

    Either<String, EntityDescriptor> resolveEntity(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException;
}
